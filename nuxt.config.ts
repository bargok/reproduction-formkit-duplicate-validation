export default defineNuxtConfig({
    modules: [
        '@formkit/nuxt',
    ],
    formkit: {
        configFile: './formkit.config.ts',
    },
})
