# Reproduction - Formkit - Double Validation

- Problem with validation: required|delayed
  - Run `yarn`
  - Run `yarn dev`
  - Open `http://localhost:3000`
  - Inspect the console, see double validation messages from ./formkit/rules/delayed.ts.


- No problem when changing to validation: delayed
  - Change validation in app.vue
  - Run `yarn dev`
  - Open `http://localhost:3000`
  - Inspect the console, no more duplicate messages.
