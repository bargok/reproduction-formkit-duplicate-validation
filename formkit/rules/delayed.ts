import { FormKitNode } from '@formkit/core'

const delayed = function (node: FormKitNode): Promise<boolean> {
  console.log('start', node.name, node.value)
  return new Promise((resolve) => {
    setTimeout(() => {
      console.log('done', node.name, node.value)
      resolve(true)
    }, 2000)
  })
}

export default delayed
