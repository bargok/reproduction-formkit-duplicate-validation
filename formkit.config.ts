import { DefaultConfigOptions } from '@formkit/vue'
import delayed from '~/formkit/rules/delayed'

const config: DefaultConfigOptions = {
  rules: {
    delayed,
  },
}

export default config
